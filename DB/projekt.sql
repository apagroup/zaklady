-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 12 Kwi 2019, 15:45
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projektzaklady`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kupony`
--

CREATE TABLE `kupony` (
  `id_kuponu` int(11) NOT NULL,
  `id_uzytkownika` int(11) NOT NULL,
  `postawiona_suma` decimal(10,2) NOT NULL,
  `wygrana_suma` decimal(10,2) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `kupony`
--

INSERT INTO `kupony` (`id_kuponu`, `id_uzytkownika`, `postawiona_suma`, `wygrana_suma`, `status`) VALUES
(1, 1, '20.00', '0.00', 'Oczekuje'),
(2, 1, '12.00', '0.00', 'Oczekuje'),
(3, 1, '30.00', '0.00', 'Oczekuje'),
(4, 2, '100.00', '0.00', 'Oczekuje'),
(5, 2, '50.00', '0.00', 'Oczekuje'),
(6, 3, '4.00', '0.00', 'Oczekuje'),
(7, 1, '4.00', '0.00', 'Oczekuje');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mecze`
--

CREATE TABLE `mecze` (
  `id` int(11) NOT NULL,
  `gospodarz` text NOT NULL,
  `gosc` text NOT NULL,
  `bramkiGospodarz` varchar(3) DEFAULT NULL,
  `bramkiGosc` varchar(3) DEFAULT NULL,
  `dataSpotkania` date NOT NULL,
  `godzinaSpotkania` time NOT NULL,
  `wpisany_wynik` tinyint(1) NOT NULL,
  `czy_aktualizowana` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `mecze`
--

INSERT INTO `mecze` (`id`, `gospodarz`, `gosc`, `bramkiGospodarz`, `bramkiGosc`, `dataSpotkania`, `godzinaSpotkania`, `wpisany_wynik`, `czy_aktualizowana`) VALUES
(1, 'Legia Warszawa', 'Lech Poznan', '2', '2', '2019-03-21', '21:14:00', 1, 1),
(2, 'Gornik Zabrze', 'Piast Gliwice', '3', '1', '2019-03-22', '11:11:00', 1, 1),
(3, 'Arka Gdynia', 'Wisla Krakow', '1', '2', '2019-03-27', '15:45:00', 1, 1),
(4, 'Wisla Krakow', 'Lech Poznan', '5', '3', '2019-03-18', '20:30:00', 1, 1),
(5, 'Lechia Gdansk', 'Gornik Zabrze', '1', '2', '2019-03-15', '20:45:00', 1, 1),
(6, 'Wisla Krakow', 'Legia Warszawa', '0', '0', '2019-04-13', '23:41:00', 0, 0),
(7, 'Lech Poznan', 'Piast Gliwice', '0', '0', '2019-04-13', '15:15:00', 0, 0),
(8, 'Lechia Gdansk', 'Arka Gdynia', '3', '4', '2019-04-13', '05:54:00', 1, 1),
(9, 'Piast Gliwice', 'Legia Warszawa', '0', '0', '2019-04-12', '20:45:00', 0, 0),
(10, 'Lech Poznan', 'Arka Gdynia', '3', '1', '2019-04-12', '15:30:00', 1, 1),
(11, 'Gornik Zabrze', 'Wisla Krakow', '0', '0', '2019-04-14', '18:20:00', 0, 0),
(12, 'Legia Warszawa', 'Arka Gdynia', '0', '0', '2019-04-14', '11:11:00', 0, 0),
(13, 'Lechia Gdansk', 'Arka Gdynia', '0', '0', '2019-04-13', '06:06:00', 0, 0),
(15, 'Piast Gliwice', 'Lech Poznan', '0', '0', '2019-04-20', '14:14:00', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `obstawione_mecze`
--

CREATE TABLE `obstawione_mecze` (
  `id` int(11) NOT NULL,
  `id_kuponu` int(11) NOT NULL,
  `id_meczu` int(11) NOT NULL,
  `wybor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `obstawione_mecze`
--

INSERT INTO `obstawione_mecze` (`id`, `id_kuponu`, `id_meczu`, `wybor`) VALUES
(1, 1, 9, 'remis'),
(2, 1, 8, 'Lechia Gdansk'),
(3, 1, 7, 'Lech Poznan'),
(4, 1, 6, 'remis'),
(5, 2, 11, 'remis'),
(6, 2, 15, 'Lech Poznan'),
(7, 3, 12, 'remis'),
(8, 4, 9, 'Piast Gliwice'),
(9, 4, 6, 'Legia Warszawa'),
(10, 5, 8, 'remis'),
(11, 5, 11, 'Gornik Zabrze'),
(12, 5, 15, 'Lech Poznan'),
(13, 6, 8, 'Lechia Gdansk'),
(14, 6, 7, 'Piast Gliwice'),
(15, 6, 6, 'remis'),
(16, 7, 8, 'Arka Gdynia'),
(17, 7, 12, 'Legia Warszawa'),
(18, 7, 11, 'Wisla Krakow');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tabela`
--

CREATE TABLE `tabela` (
  `id` int(11) NOT NULL,
  `druzyna` text NOT NULL,
  `liczba_meczow` int(2) NOT NULL,
  `bramki_zdobyte` int(3) NOT NULL,
  `bramki_stracone` int(3) NOT NULL,
  `suma_punktow` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `tabela`
--

INSERT INTO `tabela` (`id`, `druzyna`, `liczba_meczow`, `bramki_zdobyte`, `bramki_stracone`, `suma_punktow`) VALUES
(1, 'Legia Warszawa', 3, 4, 3, 5),
(2, 'Lech Poznan', 5, 11, 9, 8),
(3, 'Gornik Zabrze', 3, 7, 3, 9),
(4, 'Piast Gliwice', 3, 2, 5, 1),
(5, 'Arka Gdynia', 5, 11, 14, 6),
(6, 'Wisla Krakow', 4, 8, 6, 7),
(7, 'Lechia Gdansk', 3, 7, 10, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `birth` date NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `birth`, `email`) VALUES
(1, 'andrzej', '$2y$10$RcAbgOF0E5mc8a/IW6Xh6ODBnF4v05ZVoWCzHZVcg5nhQTGoDgby2', '2019-04-10 12:08:56', '2019-04-12', 'andrzej@andrzej.pl'),
(2, 'cos', '$2y$10$B/rlLzpD8Z9wjYK77a2MBuaQ3qpZY8sObPTBVcKKOFdCIlUSqUQrS', '2019-04-10 13:07:57', '1995-03-02', 'cos@cos.pl'),
(3, 'sad', '$2y$10$YXP/WAEd7JJ2XvOHTADNuuFptVMSExJ67GWtlAwFgCl5b4A2uqQ02', '2019-04-10 14:54:25', '2019-04-24', '123@123.pl');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kupony`
--
ALTER TABLE `kupony`
  ADD PRIMARY KEY (`id_kuponu`);

--
-- Indeksy dla tabeli `mecze`
--
ALTER TABLE `mecze`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `obstawione_mecze`
--
ALTER TABLE `obstawione_mecze`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `tabela`
--
ALTER TABLE `tabela`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kupony`
--
ALTER TABLE `kupony`
  MODIFY `id_kuponu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `mecze`
--
ALTER TABLE `mecze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `obstawione_mecze`
--
ALTER TABLE `obstawione_mecze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT dla tabeli `tabela`
--
ALTER TABLE `tabela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
