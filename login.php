<?php

session_start();
 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 
$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}

$username = $password = "";
$username_err = $password_err = "";
 

if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    
    if(empty(trim($_POST["username"]))){
        $username_err = "Podaj nazwę użytkownika:";
    } else{
        $username = trim($_POST["username"]);
    }
    
   
    if(empty(trim($_POST["password"]))){
        $password_err = "Podaj hasło:";
    } else{
        $password = trim($_POST["password"]);
    }
    
   
    if(empty($username_err) && empty($password_err)){
        
        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($db, $sql)){
            
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            
            $param_username = $username;
            
            
            if(mysqli_stmt_execute($stmt)){
            
                mysqli_stmt_store_result($stmt);
                
                
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            
                            session_start();
                            
                            
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            
                            header("location: index.php");
                        } else{
                           
                            $password_err = "Hasło nieprawidłowe";
                        }
                    }
                } else{
                    
                    $username_err = "Nie ma takiego konta!";
                }
            } else{
                echo "Błąd... Coś poszło nie tak.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    
    mysqli_close($db);
}
?>
 
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    
    </head>
<body>
    <div>
        <h2>Login</h2>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>>
                <label>Nazwa użytkownika:</label>
                <input type="text" name="username"  value="<?php echo $username; ?>">
                <span <?php echo $username_err; ?>> </span>
            </div>    
            <div <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>>
                <label>Hasło:</label>
                <input type="password" name="password" >
                <span><?php echo $password_err; ?></span>
            </div>
            <div>
                <input type="submit"  value="Login">
            </div>
            <p>Nie masz u nas konta? <a href="register.php">Zarejestruj się!</a>.</p>
        </form>
    </div>    
</body>
</html>