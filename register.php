<html>
	<head></head>
	<title>Rejestracja</title>
		<?php
		
			$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
				if($db===false){
					die("Error:".mysqli_connect_error());
				}
			$username = $password = $confim_password = $email = $birth = "";
			$username_err = $password_err = $confirm_password_err = $email_err = $birth_err = "";
			
			if($_SERVER["REQUEST_METHOD"] == "POST"){
 
			
			if(empty(trim($_POST["username"]))){
				$username_err = "Podaj nazwę użytkownika!.";
			} else{
				
				$sql = "SELECT id FROM users WHERE username = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_username);
					
					$param_username = trim($_POST["username"]);
								
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$username_err = "Nazwa użytkownika zajęta.";
						} else{
							$username = trim($_POST["username"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
		
			if(empty(trim($_POST["password"]))){
				$password_err = "Podaj hasło.";     
			} elseif(strlen(trim($_POST["password"])) < 6){
				$password_err = "Hasło musi posiadać conajmniej 6 znaków!.";
			} else{
				$password = trim($_POST["password"]);
			}
			
			if(empty(trim($_POST["confirm_password"]))){
				$confirm_password_err = "Podaj ponownie hasło.";     
			} else{
				$confirm_password = trim($_POST["confirm_password"]);
				if(empty($password_err) && ($password != $confirm_password)){
					$confirm_password_err = "Hasła różnią się.";
				}
			}
			
			if(empty(trim($_POST["email"]))){
				$email_err = "Podaj email!.";
			} else{
				
				$sql = "SELECT id FROM users WHERE email = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_email);
					
					$param_email = trim($_POST["email"]);
							
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$email_err = "Email zajęty.";
						} else{
							$email = trim($_POST["email"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
		
		if(empty(trim($_POST["birth"]))){
				$birth_err = "Podaj datę urodzenia!.";
			} else{
				
				$sql = "SELECT id FROM users WHERE birth = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_birth);
					
					$param_birth = trim($_POST["birth"]);
						
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if((strtotime(date("Y-m-d"))-strtotime($_POST["birth"])/(60*60*24)/365)<18){
							$birth_err = "Jesteś niepełnoletni";
						} else{
							$birth = trim($_POST["birth"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
			
			if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($email_err) && empty($birth_err)){
				
				$sql = "INSERT INTO users (username, password, birth, email) VALUES (?, ?, ?, ?)";
				 
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "ssss", $param_username, $param_password, $param_birth, $param_email);
					
					$param_username = $username;
					$param_password = password_hash($password, PASSWORD_DEFAULT);
					$param_birth= $birth;
					$param_email = $email;	
					
					if(mysqli_stmt_execute($stmt)){
						header("location: login.php");
					} else{
						echo "Błąd... Spróbuj jeszcze raz";
					}
				}	
				mysqli_stmt_close($stmt);
			}
			mysqli_close($db);
		}
		
		
		
		?>
			<body>
				<div>
				<h2>Zarejestruj się!</h2>
				
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
					<div  <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>>
						<label>Nazwa użytkownika:</label>
						<input type="text" name="username" value="<?php echo $username; ?>">
						<span ><?php echo $username_err; ?></span>
					</div>    
					<div  <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>>
						<label>Hasło:</label>
						<input type="password" name="password" value="<?php echo $password; ?>">
						<span ><?php echo $password_err; ?></span>
					</div>
					<div  <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>>
						<label>Potwierdz Hasło:</label>
						<input type="password" name="confirm_password" value="<?php echo $confirm_password; ?>">
						<span ><?php echo $confirm_password_err; ?></span>
					</div>
					<div   <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>>
						<label>Podaj email:<label>
						<input type="email" name="email" value="<?php echo $email; ?>">
						<span><?php echo $email_err; ?></span>
					</div>
					<div   <?php echo (!empty($birth_err)) ? 'has-error' : ''; ?>>
						<label>Podaj datę urodzenia:<label>
						<input type="date" name="birth" value="<?php echo $birth; ?>">
						<span><?php echo $birth_err; ?></span>
					</div>
					<div>
						<input type="submit"  value="Zarejestruj">
						<input type="reset"  value="Reset">
					</div>
					<p>Masz już konto? <a href="login.php">Zaloguj się tutaj!</a></p>
				</form>
				</div>    
			</body>