<html>
<body>
	<div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>
</body>
</html>
<?php
	$baza = @new mysqli("localhost", "root", "", "projektzaklady");
	
	$miejsce=1;
	$color = 'blue';
	
	$wynik = mysqli_query($baza,"SELECT * FROM tabela order by suma_punktow desc");
	$ile = mysqli_num_rows($wynik);
	
	echo "<table>
			<thead>
				<tr>
					<td></td><td><b>Drużyna</b></td><td><b>Mecze</b></td><td><b>Bramki +</b></td><td><b>Bramki -</b></td><td><b>Punkty</b></td>
				</tr>
			</thead>";
	while($row = mysqli_fetch_array($wynik))
	{
		echo "<tbody>";
		echo "<tr>";
		echo "<td><font color='$color'><b>".$miejsce.". </font></b></td>";
		echo "<td><font color='$color'>".$row['druzyna']."</font></td>";
		echo "<td>".$row['liczba_meczow']."</td>";
		echo "<td>".$row['bramki_zdobyte']."</td>";
		echo "<td>".$row['bramki_stracone']."</td>";
		echo "<td>".$row['suma_punktow']."</td></tr></tbody>";
		
		$miejsce++;
		if($miejsce > 1 && $miejsce <= $ile-2)
			$color ='black';
		elseif ($miejsce >= $ile-2)
					$color = 'red';
	}
	echo "</table>";
	echo "<br><br><a href='index.php'>Powrót do strony głównej</a>";
?>

