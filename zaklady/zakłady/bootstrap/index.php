<!doctype html>

<html>
	<head>
	<!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">
	
		<title>Typowy typer</title>
	</head>
	
	  
	  <body id="page-top">
	  <?php
	  session_start();
			if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
				header("location: login.php");
			exit;
			}
			
		?>
			
	  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Zakłady Typowy Typer</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          
          <li class="nav-item">
		  <?php if($_SESSION['id']==4): ?>
				<li class='nav-item'>
					<a class='nav-link js-scroll-trigger' href='form.php'>Dodaj mecz</a>
				</li>
		<?php endif; ?>
		
			<li class="nav-item">
				<a class="nav-link js-scroll-trigger" href="wyniki.php">Wyświetlanie meczów</a> 
			</li>
			<li class="nav-item">
				<a class="nav-link js-scroll-trigger" href="tabela.php">Tabela</a>
			</li>
			<li class="nav-item">
				<a class="nav-link js-scroll-trigger" href="obstawianie.php">Obstawianie meczów</a>
			</li>
			<li class="nav-item">
				<a class="nav-link js-scroll-trigger" href="statystyki.php">Statystyki kuponów</a>
			</li>
			
			<li class="nav-item">
				<a class="nav-link js-scroll-trigger" href="logout.php">Wyloguj <?php echo " ". htmlspecialchars($_SESSION["username"]); ?></a>
			</li>
		   
        </ul>
      </div>
    </div>
  </nav>
	  <header class="masthead">
	    </header>
	
	
	<body>
	    <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2019 - Zakłady Typowy Typer</div>
    </div>
  </footer>
			
		 <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/creative.min.js"></script>
		
		
		
	</body>
</html>