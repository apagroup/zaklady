<!doctype html>
<html>
<center>
<head>
	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
   
   <link href="css/creative.min.css" rel="stylesheet">
  
		
		
</head>

      <body>
          <header class="masthead">
	      <div class="text-white-75 font-weight-light mb-5">
	
		     <div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>
	

<?php
	$baza = @new mysqli("localhost", "root", "", "projektzaklady");
	
	$licznik=0;
	$kurs_przeliczony= 0.0;
	$kurs_team_a = 1.2;
	$kurs_team_b = 2.3;
	$kurs_remis = 2.5;
	$id_uzytkownika = $_SESSION['id'];
	
	$zapytanie = mysqli_query($baza,"select distinct dataSpotkania from mecze where wpisany_wynik=0 order by dataSpotkania, godzinaSpotkania ASC");
	
	echo "<form action='' method='POST'>";
	
	
		while($row = mysqli_fetch_array($zapytanie))
		{
			$dzien = mysqli_query($baza,"select * from mecze where dataSpotkania='".$row['dataSpotkania']."' and wpisany_wynik=0 order by dataSpotkania, godzinaSpotkania ASC");
			echo "
			<table style='border: 1px solid black;'>
				<thead>
					<tr>
						<td>".$row['dataSpotkania']."</td>
					</tr>
				</thead>
			";
			while($row2 = mysqli_fetch_array($dzien))
			{
				echo "
				<tbody>
					<tr>
						<td width='150px'>".$row2['godzinaSpotkania']."</td>
						<td width='250px'><input type='radio' name='typ$licznik' value='".$row2['gospodarz']."'> ".$row2['gospodarz']." $kurs_team_a</td>
						<td width='150px'><input type='radio' name='typ$licznik' value='remis'>Remis $kurs_remis</td>
						<td width='250px'><input type='radio' name='typ$licznik' value='".$row2['gosc']."'>".$row2['gosc']." $kurs_team_b</td>
					</tr>
				</tbody>
				";
				$zap = mysqli_query($baza, "select * from mecze where gospodarz='".$row2['gospodarz']."' and gosc='".$row2['gosc']."'");
				$id = mysqli_fetch_array($zap);
				$id_meczow[$licznik] = $id['id'];
				$licznik++;
			}
			echo "</table>";
		}
		echo "
				</br>Stawka: <input type='text' name='stawka' placeholder='00,00 PLN'>
				<input type='submit' value='Obstaw' name='obstaw'>
				";
	
	//if($_SERVER["REQUEST_METHOD"]== "POST"){
	//if(!isset($_POST['typ0'] && $_POST['typ1'] && $_POST['typ2'] && $_POST['typ3'] && $_POST['typ4'] && $_POST['typ5'] && $_POST['stawka']))	
			if(isset($_POST['obstaw']))
			{
				$stawka = $_POST['stawka'];
				if($stawka)
				mysqli_query($baza,"INSERT INTO `kupony` (`id_kuponu`, `id_uzytkownika`, `postawiona_suma`, `wygrana_suma`, `status`) VALUES (NULL, '".$id_uzytkownika."', '".$stawka."', '".$stawka*$kurs_przeliczony."', 'Oczekuje')");
				
				$z =  mysqli_query($baza, "select * from kupony where id_uzytkownika='".$id_uzytkownika."' order by id_kuponu desc limit 1");
				$id_k = mysqli_fetch_array($z);
				$id_kuponu = $id_k['id_kuponu'];
				
				for($i=0; $i<=$licznik; $i++)
				{
					if(isset($_POST['typ'.$i]))
					{
						//echo "Kupon id: ".$id_kuponu." mecz id: ".$id_meczow[$i]." typ: ".$_POST['typ'.$i].". </br>";
						mysqli_query($baza, "INSERT INTO `obstawione_mecze` (`id`, `id_kuponu`, `id_meczu`, `wybor`) VALUES (NULL, '".$id_kuponu."', '".$id_meczow[$i]."', '".$_POST['typ'.$i]."')");
					}
				}
				echo "Kupon postawiony!";
			}
	
	
		
	mysqli_close($baza);
	echo "<br><br><a href='index.php'>Powrót do strony głównej</a>";
?>
</form>
</body>
<center>
</html>