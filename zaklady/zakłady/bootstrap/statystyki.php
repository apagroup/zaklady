<!doctype html>
<html>
<center>
<head>
	<!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">
		
		
	</head>
	<body>
	<header class="masthead">
	<div class="text-white-75 font-weight-light mb-5">
		<div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>
	
<?php
	$baza = @new mysqli("localhost", "root", "", "projektzaklady");
	$id_uzytkownika=$_SESSION['id'];
	$licznik=1;
	
	$kupon = mysqli_query($baza,"select * from kupony where id_uzytkownika = '".$id_uzytkownika."'");
	
	
	echo "
		<table style='border: 1px solid black;'>
			<thead>
				<tr>
					<td>Kupon</td>
					<td>Postawiona suma</td>
					<td>Możliwa wygrana</td>
					<td>Status</td>
					<td></td>
				</tr>
			</thead>
		";
	while($row = mysqli_fetch_array($kupon))
	{
		echo" 
		<tbody>
			<tr>
				<td>".$licznik."</td>
				<td>".$row['postawiona_suma']."</td>
				<td>".$row['wygrana_suma']."</td>
				<td>".$row['status']."</td>
				<td>
					<form method='POST' action='kupon_szczegoly.php'>
						<button type='submit' value=".$row['id_kuponu']." name='szczegoly_kuponu'>Szczegóły</button>
					</form>
				</td>
			</tr>
		</tbody>
		";
		$licznik++;
	}
	echo "</table>";
	echo "<br><br><a href='index.php'>Powrót do strony głównej</a>";
?>
</body>
	</center>
</html>
