<html>
<center>
<head>
	<!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">
		
		
	</head>

<body class="text-white-75 font-weight-light mb-5">
    <header class="masthead">
	<div>
	<div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>

<?php
	$baza = @new mysqli("localhost", "root", "", "projektzaklady");
	
	$miejsce=1;
	$color = 'blue';
	
	$wynik = mysqli_query($baza,"SELECT * FROM tabela order by suma_punktow desc");
	$ile = mysqli_num_rows($wynik);
	
	echo "<table>
			<thead>
				<tr>
					<td></td><td><b>Drużyna</b></td><td><b>Mecze</b></td><td><b>Bramki +</b></td><td><b>Bramki -</b></td><td><b>Punkty</b></td>
				</tr>
			</thead>";
	while($row = mysqli_fetch_array($wynik))
	{
		echo "<tbody>";
		echo "<tr>";
		echo "<td><font color='$color'><b>".$miejsce.". </font></b></td>";
		echo "<td><font color='$color'>".$row['druzyna']."</font></td>";
		echo "<td>".$row['liczba_meczow']."</td>";
		echo "<td>".$row['bramki_zdobyte']."</td>";
		echo "<td>".$row['bramki_stracone']."</td>";
		echo "<td>".$row['suma_punktow']."</td></tr></tbody>";
		
		$miejsce++;
		if($miejsce > 1 && $miejsce <= $ile-2)
			$color ='white';
		elseif ($miejsce >= $ile-2)
					$color = 'red';
	}
	echo "</table>";
	echo "<br><br><a href='index.php'>Powrót do strony głównej</a>";
?>

</body>
<center>
</html>