<!doctype html>

<html>
<center>
	<head>
	<!-- Font Awesome Icons -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.min.css" rel="stylesheet">
		<title>Mecze</title>
		
	</head>

	<body>
	
	<header class="masthead">
	<div class="text-white-75 font-weight-light mb-5">
	<div align='right'>
		<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
		<a href='logout.php'>Wyloguj</a>
		
	</div>

	<?php
		
		//$baza = @new mysqli("localhost", "root", "", "projektzaklady1");
		$baza =mysqli_connect("localhost", "root", "");
		mysqli_select_db($baza,"projektzaklady");
		if($baza->connect_errno != 0)
		{
			echo "Error: ".$baza->connect_errno;
		}
		 
		$na_stronie = 5; 

		$wynik = mysqli_query($baza,'SELECT COUNT(id) FROM mecze');
		$a = mysqli_fetch_row($wynik);
		$liczba_wpisow = $a[0];
		$liczba_stron = ceil($liczba_wpisow / $na_stronie);
    

    if (isset($_GET['strona'])) {

        if ($_GET['strona'] < 1 || $_GET['strona'] > $liczba_stron) $strona = 1;

        else $strona = $_GET['strona'];

    }
    else $strona = 1;

    $od = $na_stronie * ($strona - 1);
	
    $wynik = mysqli_query($baza,"SELECT * FROM mecze ORDER BY id DESC LIMIT $od , $na_stronie");

    while ($row = mysqli_fetch_array($wynik)) 
	{
		if($row['wpisany_wynik'] == 0)
		{
			echo "Nierozegrany -> ";
		}else
		{
			echo "Zakończony -> ";
		}
		
		echo "Mecz: ".$row['gospodarz'] . " - " . $row['gosc']." | Wynik: ".$row['bramkiGospodarz']." - ".$row['bramkiGosc']." | Data: ".$row['dataSpotkania']." ".$row['godzinaSpotkania'];
			echo "<form method='post' action='szczegoly.php'>
			<button type='submit' value=".$row['id']." name='szczegoly'>Szczegóły</button></form>";
			if($_SESSION['id']==1)
				echo "<form method='post' action='wpiszWynik.php'><button type='submit' value=".$row['id']." name='wpiszWynik'>Wpisz wynik</button></form><br>";
    }


    if ($liczba_wpisow > $na_stronie) {

        $poprzednia = $strona - 1;

        $nastepna = $strona + 1;

        if ($poprzednia > 0) {

			echo '<a id="POPRZEDNIA" href="wyniki.php?strona='.$poprzednia.'">poprzednia strona </a>';

        }
			for($i=0;$i<$liczba_stron;$i++)
			{
				//echo " [". ($i+1).'<a href"=?strona='.($_GET['strona']+$i).">  ] |;
			}
		
        if ($nastepna <= $liczba_stron) {
			echo '<a id="NASTEPNA" href="wyniki.php?strona='.$nastepna.'">następna strona</a>';

        }
	}
		if($_SESSION['id']==1)
		{
			echo "
			<form action='aktualizacjaTab.php' method='POST'>
				<button type='submit'>Aktualizacja wynikow</button>
			</form>
			";
		}
		echo "<br><br><a href='index.php'>Powrót do strony głównej</a>";
	?>
	
	</body>
	</center>
</html>