<html>
	<head>
		<title>Zmień Hasło</title>
	</head>
	<body>
		<div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>
	<?php
		$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
		if($db===false){
			die("Error:".mysqli_connect_error());
		}
		
		$old_password = $new_password = $confim = "";
		$old_err = $new_err = $confim_err = $komunikat = "";
		
	if($_SERVER["REQUEST_METHOD"]== "POST"){
		
		if(empty(trim($_POST["old"])))
				$old_err = "Podaj hasło.";     
			else
				$old_password = trim($_POST["old"]);
			
			
			if(empty(trim($_POST["new"]))){
				$new_err = "Podaj hasło.";     
			} elseif(strlen(trim($_POST["new"])) < 6){
				$new = "Hasło musi posiadać conajmniej 6 znaków!.";
			} else{
				$new_password = trim($_POST["new"]);
			}
			
			if(empty(trim($_POST["confim"]))){
				$confim_err = "Podaj ponownie hasło.";     
			} else{
				$confim = trim($_POST["confim"]);
				if(empty($new_err) && ($new_password != $confim)){
					$confim_err = "Hasła różnią się.";
				}
			}
			
		
		if(empty($old_err) && empty($new_err) && empty($confim_err)){
			
			$zapyt = "SELECT id,username,password FROM users WHERE username = ?";
			if($stmt = mysqli_prepare($db, $zapyt)){
				
				mysqli_stmt_bind_param($stmt,"s",$param_username);
				
				$param_username = $_SESSION["username"];
				
				if(mysqli_stmt_execute($stmt)){
            
					mysqli_stmt_store_result($stmt);
					
					if(mysqli_stmt_num_rows($stmt) == 1){                    
                    
						mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
						if(mysqli_stmt_fetch($stmt)){
							if(password_verify($old_password, $hashed_password)){
								
								$zmiana = "UPDATE users SET password = ? WHERE username = ?";
								if($stmt = mysqli_prepare($db,$zmiana)){
									
									mysqli_stmt_bind_param($stmt,"ss",$param_new,$param_username);
									$param_new = password_hash($new_password,PASSWORD_DEFAULT);
									$param_username = $_SESSION["username"];
									if(mysqli_stmt_execute($stmt)){
										$komunikat = "Haslo zmienione!";
									}else
										$komunikat = "Błąd!";
								}			
								
							}else 
								$old_err="Błędne hasło!";	
						}        
					}
				}
			}
			
		}
		mysqli_close($db);	
	}		
	?>	
	<form action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="POST">
	<label>Stare haslo:</label>
	<input type="password" name="old" >
	<span><?php echo $old_err; ?></span><br>
	<label>Nowe haslo:</label>
	<input type="password" name="new">
	</span><?php echo $new_err; ?><br>
	<label>Powtórz nowe haslo:</label>
	<input type="password" name="confim">
	<span><?php echo $confim_err; ?><br>
	<input type="submit" value="Zmień">
	<span><br><?php echo $komunikat; ?></span>
	</form>
	<br><br>
	<a href="uzytkownik.php">Powrót do panelu użytkownika</a><br>
	<a href="index.php">Strona główna</a> 
</html>