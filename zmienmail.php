<html>
	<head>
		<title>Zmień Mail</title>
	</head>
	<body>
		<div align='right'>
				<?php
					session_start();
					if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
						header("location: login.php");
						exit;
					}
					echo "Witaj ".htmlspecialchars($_SESSION["username"]);
				?>
				<a href='logout.php'>Wyloguj</a>
			</div>
	<?php
		$db=mysqli_connect("localhost", "root","" ,"projektzaklady");
		if($db===false){
			die("Error:".mysqli_connect_error());
		}
		$new_mail = $confim_mail = "";
		$mail_err = $confim_err = $komunikat = "";
		
		if($_SERVER["REQUEST_METHOD"] == "POST"){
			
			if(empty(trim($_POST["new"])))
				$mail_err = "Podaj mail!";
			else{
				$sql = "SELECT id FROM users WHERE email = ?";
				
				if($stmt = mysqli_prepare($db, $sql)){
					
					mysqli_stmt_bind_param($stmt, "s", $param_email);
					
					$param_email = trim($_POST["new"]);
							
					if(mysqli_stmt_execute($stmt)){
						
						mysqli_stmt_store_result($stmt);
						
						if(mysqli_stmt_num_rows($stmt) == 1){
							$mail_err = "Email zajęty.";
						} else{
							$new_mail = trim($_POST["new"]);
						}
					} else{
						echo "Błąd... Spróbuj jeszcze raz.";
					}
				}
				mysqli_stmt_close($stmt);
			}
			
			if(empty(trim($_POST["confim"])))
				$confim_err = "Podaj mail!";
			else{ 
				$confim_mail = trim($_POST["confim"]);
				if(empty($mail_err) && $new_mail != $confim_mail)
				$confim_err = "Maile różnią się!";
			}
			
			if(empty($mail_err) && empty($confim_err)){
			
			
				$zapyt = "UPDATE users SET email = ? WHERE username = ?";
					if($stmt = mysqli_prepare($db,$zapyt)){
						mysqli_stmt_bind_param($stmt,"ss",$param_mail,$param_username);
						$param_mail = $new_mail;
						$param_username = $_SESSION["username"];
									
						if(mysqli_stmt_execute($stmt))
							$komunikat = "Zmieniono pomyślnie";
						else
							$komunikat = "Błąd!";
					}	
			}	
							
		}	
			
	?>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
	<label>Podaj nowego maila:</label>
	<input type="email" name="new">
	<span><?php echo $mail_err; ?><span><br>
	<label>Podaj ponownie maila:</label>
	<input type="email" name="confim">
	<span><?php echo $confim_err; ?></span><br>
	<input type="submit" value="Zmień"><br>
	<span><?php echo $komunikat; ?></span>
	
	</form>
	<br><br>
	<a href="uzytkownik.php">Powrót do panelu użytkownika</a><br>
	<a href="index.php">Strona główna</a> 
	</body>	
</html>	